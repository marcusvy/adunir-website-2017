import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AdunirComponent} from "./adunir.component";
import {HomeComponent} from './home/home.component';
import {AgendaComponent} from "./agenda/agenda.component";
import {ParceiroComponent} from './parceiro/parceiro.component';
import {NoticiaComponent} from './noticia/noticia.component';
import {LegislacaoComponent} from './legislacao/legislacao.component';
import {FiliacaoComponent} from './filiacao/filiacao.component';
import {SobreComponent} from "./sobre/sobre.component";

const routes: Routes = [
  {
    path: '',
    component: AdunirComponent,
    children: [
      {path: '', component: HomeComponent, pathMatch: 'full'},
      {path: 'sobre', component: SobreComponent },
      {path: 'agenda', component: AgendaComponent},
      {path: 'filiacao', component: FiliacaoComponent},
      {path: 'legislacao', component: LegislacaoComponent},
      {path: 'noticia', component: NoticiaComponent},
      {path: 'parceiro', component: ParceiroComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdunirRoutingModule {
}
