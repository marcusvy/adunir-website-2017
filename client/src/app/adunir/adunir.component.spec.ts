/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AdunirComponent } from './adunir.component';

describe('AdunirComponent', () => {
  let component: AdunirComponent;
  let fixture: ComponentFixture<AdunirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdunirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdunirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
