import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { MasksModule } from '../masks/masks.module';
import { SharedModule } from "./shared/shared.module";

import { AdunirRoutingModule } from './adunir-routing.module';

import { HomeComponent } from './home/home.component';
import { ParceiroComponent } from './parceiro/parceiro.component';
import { NoticiaComponent } from './noticia/noticia.component';
import { LegislacaoComponent } from './legislacao/legislacao.component';
import { AgendaComponent } from './agenda/agenda.component';
import { FiliacaoComponent } from './filiacao/filiacao.component';
import { AdunirComponent } from "./adunir.component";
import { SobreComponent } from './sobre/sobre.component';

@NgModule({
  imports: [
    CoreModule,
    MasksModule,
    SharedModule,
    AdunirRoutingModule,
  ],
  declarations: [
    AdunirComponent,
    HomeComponent,
    ParceiroComponent,
    NoticiaComponent,
    LegislacaoComponent,
    AgendaComponent,
    FiliacaoComponent,
    SobreComponent,
  ],
  providers: [
    { provide: 'API', useValue: '/api' },
  ]
})
export class AdunirModule {
}
