import { Component, OnInit, HostBinding } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'mv-adunir-menu',
  templateUrl: './adunir-menu.component.html',
  styleUrls: ['./adunir-menu.component.scss']
})
export class AdunirMenuComponent implements OnInit {

  @HostBinding('class.is-open')
  open = false;

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events
      .filter((event => event instanceof NavigationEnd))
      .subscribe((event) => {
        if (this.open) {
          this.toggle();
        }
      })
  }

  toggle() {
    this.open = !this.open;
  }

}
