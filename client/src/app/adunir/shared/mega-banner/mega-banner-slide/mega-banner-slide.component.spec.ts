import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MegaBannerSlideComponent } from './mega-banner-slide.component';

describe('MegaBannerSlideComponent', () => {
  let component: MegaBannerSlideComponent;
  let fixture: ComponentFixture<MegaBannerSlideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MegaBannerSlideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MegaBannerSlideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
