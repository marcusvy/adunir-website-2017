import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mv-mega-banner-slide',
  templateUrl: './mega-banner-slide.component.html',
  styleUrls: ['./mega-banner-slide.component.scss']
})
export class MegaBannerSlideComponent implements OnInit {

  @Input() title;
  @Input() description;
  @Input() image;
  @Output() onChange: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  getImageBackground() {
    let url = `url('${this.image}')`;
    this.onChange.emit(url);
    return url;
  }



}
