import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { MegaBannerService } from "./mega-banner.service";


@Component({
  selector: 'mv-mega-banner',
  templateUrl: './mega-banner.component.html',
  styleUrls: ['./mega-banner.component.scss']
})
export class MegaBannerComponent implements OnInit, OnDestroy {

  @Input() api;

  slides = [];
  private slideSubscription: Subscription;
  private service: MegaBannerService;

  constructor(service: MegaBannerService) {
    this.service = service;
  }

  ngOnInit() {
    if(this.api !== undefined) {
      this.slideSubscription = this.service.get(this.api)
        .subscribe((data) => {
          this.slides = data.collection;
        });
    }

  }

  ngOnDestroy() {
    if(this.slideSubscription){
      this.slideSubscription.unsubscribe();
    }
  }

  setup(event){

  }

}
