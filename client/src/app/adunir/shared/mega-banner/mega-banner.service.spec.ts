import { TestBed, inject } from '@angular/core/testing';

import { MegaBannerService } from './mega-banner.service';

describe('MegaBannerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MegaBannerService]
    });
  });

  it('should ...', inject([MegaBannerService], (service: MegaBannerService) => {
    expect(service).toBeTruthy();
  }));
});
