import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response } from '@angular/http';

@Injectable()
export class MegaBannerService {

  private http: Http;

  constructor(http: Http) {
    this.http = http;
  }

  get(url){
    return this.http.get(url)
      .map((res: Response) => res.json())
      .catch(this.errorHandler);
  }

  errorHandler(error: any) {
    return Observable.throw(error.json().error || 'Server error');
  }
}
