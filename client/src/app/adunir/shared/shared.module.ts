import { NgModule } from '@angular/core';
import { CoreModule } from '../../core/core.module';
import { AdunirRoutingModule } from "../adunir-routing.module";

import { MegaBannerComponent } from './mega-banner/mega-banner.component';
import { AdunirMenuComponent } from './adunir-menu/adunir-menu.component';
import { AdunirHeaderComponent } from './adunir-header/adunir-header.component';
import { AdunirFooterComponent } from './adunir-footer/adunir-footer.component';
import { TopicComponent } from './topic/topic.component';
import { AlertComponent } from './alert/alert.component';
import { TopicListDirective } from './topic/topic-list.directive';
import { TopicItemComponent } from './topic/topic-item/topic-item.component';
import { TopicNumberComponent } from './topic/topic-number/topic-number.component';
import { TopicDateComponent } from './topic/topic-date/topic-date.component';
import { TopicActionDirective } from './topic/topic-action.directive';
import { MegaBannerSlideComponent } from './mega-banner/mega-banner-slide/mega-banner-slide.component';
import { MegaBannerService } from "./mega-banner/mega-banner.service";

@NgModule({
  imports: [
    CoreModule,
    AdunirRoutingModule,
  ],
  declarations: [
    MegaBannerComponent,
    MegaBannerSlideComponent,
    AdunirMenuComponent,
    AdunirHeaderComponent,
    AdunirFooterComponent,
    TopicComponent,
    AlertComponent,
    TopicListDirective,
    TopicItemComponent,
    TopicNumberComponent,
    TopicDateComponent,
    TopicActionDirective
  ],
  exports: [
    MegaBannerComponent,
    MegaBannerSlideComponent,
    AdunirMenuComponent,
    AdunirHeaderComponent,
    AdunirFooterComponent,
    TopicComponent,
    AlertComponent,
    TopicListDirective,
    TopicItemComponent,
    TopicNumberComponent,
    TopicDateComponent,
    TopicActionDirective,
  ],
  providers:[
    MegaBannerService
  ]
})
export class SharedModule { }
