import { Component, OnInit, Input } from '@angular/core';
import  * as moment from 'moment';

@Component({
  selector: 'mv-topic-date',
  templateUrl: './topic-date.component.html',
  styleUrls: ['./topic-date.component.scss']
})
export class TopicDateComponent implements OnInit {

  @Input() date:string;

  private day;
  private month;
  private year;

  constructor() { }

  ngOnInit() {
    let date = moment(this.date).isValid() ? moment(this.date) : null;

    this.day = date.format('DD');
    this.month = date.format('MMM');
    this.year = date.format('YYYY');
  }

}
