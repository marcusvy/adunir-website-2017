import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TopicComponent implements OnInit {

  @Input() title:string = '';

  constructor() { }

  ngOnInit() {
  }

}
