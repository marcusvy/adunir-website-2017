import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { AdunirModule } from './adunir/adunir.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


/**
 * Moment
 */
import * as moment from 'moment';
import 'moment/locale/pt-br';
moment.locale('pt-br');


/**
 * WebFont
 */
import * as WebFont from 'webfontloader';

WebFont.load({
  google: {
    families: ['Roboto:100,500']
  }
});

/**
 * Aplication
 */
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    AuthModule,
    AdunirModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "pt-BR" },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
