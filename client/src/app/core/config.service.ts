import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {

  // API base path: https://
  public API = 'http://localhost:8000/api';
  // public API = 'https://urutau-api.mviniciusconsultoria.com.br/api';

  constructor() { }

}
