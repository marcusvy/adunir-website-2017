/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {CoreModule} from '../../core/core.module';
import {ControlNumberComponent} from './control-number.component';
import {ControlReactiveComponent} from '../control-reactive/control-reactive.component';
import { ControlActionDirective } from '../control-action.directive';

describe('ControlNumberComponent', () => {
  let component: ControlNumberComponent;
  let fixture: ComponentFixture<ControlNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ CoreModule ],
      declarations: [
        ControlActionDirective,
        ControlReactiveComponent,
        ControlNumberComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
