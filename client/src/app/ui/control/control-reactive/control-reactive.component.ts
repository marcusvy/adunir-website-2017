import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
} from '@angular/core';

@Component({
  selector: 'mv-control-reactive',
  templateUrl: './control-reactive.component.html',
  styleUrls: ['./control-reactive.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class ControlReactiveComponent implements OnInit{

  @Input() formControl;
  @Input() icon:string = '';
  @Input() label:string = '';
  @ViewChild('controlAction') controlAction:ElementRef;
  @ViewChild('controlSetup') controlSetup:ElementRef;

  private controlActionHidden:Boolean = true;
  private controlSetupHidden:Boolean = true;

  constructor() { }

  ngOnInit() {
    this.controlActionHidden = this.controlAction.nativeElement.childElementCount == 0;
    this.controlSetupHidden = this.controlSetup.nativeElement.childElementCount == 0;
  }

  get hasIcon() {
    return (this.icon.length > 0);
  }

  isControlActionHidden(){
    return this.controlActionHidden;
  }
  isControlSetupHidden(){
    return this.controlSetupHidden;
  }
}
