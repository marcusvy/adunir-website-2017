import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-control-switch',
  templateUrl: './control-switch.component.html',
  styleUrls: ['./control-switch.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ControlSwitchComponent implements OnInit {

  @Input() value:boolean = false;
  @Input() for;

  constructor() { }

  ngOnInit() {
  }

  getStatusClass() {
    return {
      'mv-control-switch--true' : (this.value),
      'mv-control-switch--false' : (!this.value)
    }
  }

}
