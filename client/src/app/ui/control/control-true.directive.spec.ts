/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { ControlTrueDirective } from './control-true.directive';

describe('ControlTrueDirective', () => {
  it('should create an instance', () => {
    let directive = new ControlTrueDirective();
    expect(directive).toBeTruthy();
  });
});
