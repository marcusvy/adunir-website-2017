import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
  selector: 'mv-data-list-modal-delete',
  templateUrl: './data-list-modal-delete.component.html',
  styleUrls: ['./data-list-modal-delete.component.scss']
})
export class DataListModalDeleteComponent implements OnInit {

  @Input() entity;
  @Output() onConfirmDelete = new EventEmitter();
  @ViewChild('modal') modal;

  constructor() { }

  ngOnInit() {

  }
  open() {
    this.modal.open();
  }

  close() {
    this.modal.close();
  }

  onDelete(){
    this.onConfirmDelete.emit(this.entity);
    this.modal.close();
  }

}
