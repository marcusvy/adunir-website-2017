import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
  selector: 'mv-data-list-modal-form',
  templateUrl: './data-list-modal-form.component.html',
  styleUrls: ['./data-list-modal-form.component.scss']
})
export class DataListModalFormComponent implements OnInit  {

  @Input() entity;
  @Input() modeEdit;
  @Output() onConfirmSave = new EventEmitter();
  @ViewChild('modal') modal;

  public title;

  constructor() { }

  ngOnInit() {
    this.title = (this.modeEdit) ? 'Atualizar' : 'Adicionar';
  }

  open() {
    this.modal.open();
  }

  close() {
    this.modal.close();
  }

  onSave() {
    this.onConfirmSave.emit(this.entity);
    this.modal.close();
  }
}
