import { Component, Output, EventEmitter, OnInit, ViewChild, Renderer, Input } from '@angular/core';
import { FormGroup } from '@angular/forms'

@Component({
  selector: 'mv-data-list-modal-search',
  templateUrl: './data-list-modal-search.component.html',
  styleUrls: ['./data-list-modal-search.component.scss']
})
export class DataListModalSearchComponent implements OnInit {

  submitted: boolean = false;

  @Input() form: FormGroup;
  @Output() onSearch = new EventEmitter();
  @Output() onClean = new EventEmitter();
  @ViewChild('modal') modal;

  constructor() { }

  ngOnInit() {}

  onSubmit($event) {
    this.onSearch.emit(this.form.value);
    this.onClean.emit(true);
    this.submitted = true;
    this.modal.close();
  }

  open() {
    this.modal.open();
  }

  close() {
    this.reset();
    this.modal.close();
  }

  reset() {
    this.onClean.emit(false);
    if(this.form){
      this.form.reset();
    }
  }

  isResetVisible() {
    if(this.form){
      if(this.form.contains('for')){
        return (this.form.controls['for'].dirty
        || this.form.controls['for'].valid);
      }
    }
    return false;
  }
  isFormInvalid(){
    if(this.form){
      return this.form.invalid;
    }
  }
}
