import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-layout',
  template: '',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
