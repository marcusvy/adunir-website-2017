import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';
import { IconModule } from '../icon/icon.module';
import { ToolbarModule } from '../toolbar/toolbar.module';

import { PageTitleComponent } from './page-title.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    IconModule,
    ToolbarModule,
  ],
  declarations: [
    PageTitleComponent,
  ],
  exports: [
    PageTitleComponent,
  ]
})
export class PageTitleModule { }
