<?php

namespace User\Action;

use Interop\Container\ContainerInterface;
use User\Service\UserService;
use Doctrine\ORM\EntityManager;

class ActivationPageFactory
{
  public function __invoke(ContainerInterface $container)
  {
    $entityManager = $container->get(EntityManager::class);
    $service = $container->get(UserService::class);

    return new ActivationPageAction($entityManager, $service);
  }
}
