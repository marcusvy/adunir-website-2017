<?php

namespace User\Action;

use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Doctrine\ORM\EntityManager;
use User\Service\UserService;
use User\Form\User as UserForm;

class UserPageFactory
{
  public function __invoke(ContainerInterface $container)
  {
    $router = $container->get(RouterInterface::class);
    $entityManager = $container->get(EntityManager::class);
    $service = $container->get(UserService::class);

    $formElementManager = $container->get('FormElementManager');
    $form = $formElementManager->get(UserForm::class);

    return new UserPageAction($router, $entityManager, $service, $form);
  }
}
