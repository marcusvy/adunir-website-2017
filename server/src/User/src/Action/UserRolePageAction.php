<?php

namespace User\Action;

use Core\Action\AbstractRestPageAction;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use User\Entity\UserRole;

class UserRolePageAction
  extends AbstractRestPageAction
  implements ServerMiddlewareInterface
{
  protected $entity = UserRole::class;
}
