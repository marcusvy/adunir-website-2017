<?php
namespace User\Form\Fieldset;

use User\Repository\UserRoleRepository;
use User\Service\UserRoleService;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Hydrator;
use User\Entity\UserRole;

class Role
  extends Fieldset
  implements InputFilterProviderInterface
{
  private $service;

  public function __construct(UserRoleService $service)
  {
    parent::__construct('user_role');
    $this->service = $service;
  }

  /**
   * Inicialization
   */
  public function init()
  {
    $this->setHydrator(new Hydrator\ClassMethods(true));
    $this->setObject(new UserRole());

    $options = $this->service->getNamesForSelect();

    $this->add([
      'type' => \Zend\Form\Element\Select::class,
      'name' => 'name',
      'options' => [
        'label' => 'Função',
        'value_options' => $options,
      ],
    ]);
  }

  public function getInputFilterSpecification()
  {
    return array(
      'id' => [
        'required' => true,
      ]
    );
  }
}
