<?php
namespace MvUser\Form;

use Zend\Form\Form;

class Login
  extends Form
{
  public function init()
  {
    $this->add(array(
      'name' => 'login',
      'type' => 'MvUser\Form\Fieldset\Login',
    ));

    $this->add([
      'name' => 'remember',
      'type' => 'Checkbox',
      'options'=>[
        'label'=>'Lembrar meu registro'
      ]
    ]);
  }

  public function getInputFilterSpecification()
  {
    return array(
      'remember' => array(
        'filters' => [
          ['name' => 'Zend\Filter\Boolean']
        ]
      )
    );
  }
}
